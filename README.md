Alpine - Package Repository
===========================

Repository Setup
----------------

Install with APK repository.

```
% sudo curl -o /etc/apk/keys/apkbuild-59bb1f33.rsa.pub https://gitlab.com/kjdev/alpine.pkg/raw/master/apkbuild-59bb1f33.rsa.pub
% sudo echo 'https://gitlab.com/kjdev/alpine.pkg/raw/master/apk' >> /etc/apk/repositories
```

> Dockerfile
>
> ```
> FROM alpine
> ADD https://gitlab.com/kjdev/alpine.pkg/raw/master/apkbuild-599b7978.rsa.pub /etc/apk/keys/apkbuild-599b7978.rsa.pub
> RUN echo 'https://gitlab.com/kjdev/alpine.pkg/raw/master/apk' >> /etc/apk/repositories
> ```

APK Install
-----------

```
% apk add <PACKAGE>
```
